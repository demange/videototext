#################
#
# Video2Text
# By @DemangeJeremy
#
#################

import os
import time
import click
import json
from halo import Halo
from utils.links import getHost
from utils.scraping import DailymotionGetSubtitles
from youtube_transcript_api import YouTubeTranscriptApi
from youtube_transcript_api.formatters import JSONFormatter

import logging

@click.command()
@click.option('--model', default="auto", help='Speech2Text model from HuggingFace (example: facebook/s2t-small-librispeech-asr).')
@click.option('--link', prompt='Link to the video to be extracted', help='Link to the video to be extracted.')
@click.option('--save', prompt='Where do you want to save the text file', help='Where do you want to save the text file (example: ./myfolder/fichier1).')
@click.option('--format', default="BOTH", prompt='JSON / TEXT / BOTH', help='JSON or TEXT or BOTH.')
@click.option('--debug', default=False, help='Debug.')

def toText(model, link, save, format, debug):
    """
    From video to text in seconds.
    By @DemangeJeremy
    """
    # Logs 
    if debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.CRITICAL)

    # Verifications
    if (format != "JSON" and format != "TEXT" and format != "BOTH"):
        click.echo(f"Sorry, the format {format} is not supported. Please, choose between JSON, TEXT or BOTH.")
        return
    
    # Check if the link is supported
    if (getHost(link) == ("youtube.com" or "youtu.be")):
        with Halo(text='Extracting text from YouTube...', spinner='dots'):
            try:
                # Get the video ID
                if (getHost(link) == "youtube.com"):
                    video_id = link.split('v=')[1]
                else:
                    video_id = link.split('be/')[1]
            except Exception as error:
                if debug:
                    print(error)
                click.echo(f"Sorry, the video ID is not available. Please, check your link.")
                return

            try:
                # Get the transcript
                transcript = YouTubeTranscriptApi.get_transcript(video_id, languages=["fr", "en"])
                formatter = JSONFormatter()

                # .format_transcript(transcript) turns the transcript into a JSON string.
                json_formatted = formatter.format_transcript(transcript)

                # Now we can write it out to a file.
                # Create folders if they don't exist
                os.makedirs(os.path.dirname(save), exist_ok=True)
                if format == "JSON" or format == "BOTH":
                    with open(f'{save}.json', 'w', encoding='utf-8') as json_file:
                        json_file.write(json_formatted)
                if format == "TEXT" or format == "BOTH":
                    with open(f'{save}.txt', 'w', encoding='utf-8') as txt_file:
                        for line in transcript:
                            txt_file.write(line['text'] + '\n')
            except Exception as error:
                if debug:
                    print(error)
                click.echo(f"Sorry, the video transcript is not available.")
                return
    elif (getHost(link) == "dailymotion.com"):
        transcript = DailymotionGetSubtitles(link)
        # Now we can write it out to a file.
        # Create folders if they don't exist
        os.makedirs(os.path.dirname(save), exist_ok=True)
        if format == "JSON" or format == "BOTH":
            with open(f'{save}.json', 'w', encoding='utf-8') as json_file:
                json_file.write(json.dumps(transcript))
        if format == "TEXT" or format == "BOTH":
            with open(f'{save}.txt', 'w', encoding='utf-8') as txt_file:
                for line in transcript:
                    txt_file.write(line['text'] + '\n')

    else:
        logging.warning("Host problem:", getHost(link))
        click.echo(f"Sorry, this link is not supported yet.")
        return

if __name__ == '__main__':
    toText()