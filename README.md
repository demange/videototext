# Video To Text

Transform videos into text.

## Installation

...

## Installation from sources

```bash 
git clone https://git.etis-lab.fr/demange/videototext
cd video-to-text
pip install .
```

## Usage

```bash
video2text --help
```
