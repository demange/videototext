#################
#
# Video2Text
# By @DemangeJeremy
#
# - Scraping 
#
#################

from trafilatura import fetch_url, extract
import json
import re

def DailymotionGetSubtitles(video):
    # Dailymotion
    # https://www.dailymotion.com/video/x8oe8ke
    # https://www.dailymotion.com/player/metadata/video/x8oe8ke
    # https://static2.dmcdn.net/static/video/874/407/524704478_subtitle_fr-auto.srt?rev=0

    # 1 Get the video ID
    getVideoId = video.split('video/')[1]

    # 2 Get the metadata
    getMetadataUrl = f"https://www.dailymotion.com/player/metadata/video/{getVideoId}"
    getMetadata = fetch_url(getMetadataUrl)
    getMetadata = json.loads(getMetadata)
    if getMetadata["subtitles"]["enable"]:
        try:
            getSubtitlesUrl = getMetadata["subtitles"]["data"]["fr-auto"]["urls"][0]
            getSubtitles = fetch_url(getSubtitlesUrl)
            # Extraction des sous-titres
            subtitles = re.findall(r'(\d+)\n(\d{2}:\d{2}:\d{2},\d{3}) --> (\d{2}:\d{2}:\d{2},\d{3})\n(.+?)\n\n', getSubtitles, re.DOTALL)

            # Transformation des sous-titres en format JSON
            json_subs = []
            for _, (_, start, end, text) in enumerate(subtitles):
                start_time = sum(x * float(t.replace(',', '.')) for x, t in zip([3600, 60, 1], start.split(':')))
                end_time = sum(x * float(t.replace(',', '.')) for x, t in zip([3600, 60, 1], end.split(':')))
                duration = end_time - start_time
                json_subs.append({'text': text, 'start': start_time, 'duration': duration})

            # Affichage du résultat en format JSON
            return json_subs
        except:
            return
    return

def ArteGetSubtitles(video):
    # https://www.arte.tv/fr/videos/110342-105-A/le-dessous-des-images/
    # https://api.arte.tv/api/player/v2/config/fr/110342-105-A
    # https://arte-cmafhls.akamaized.net/am/cmaf/110000/110300/110342-105-A/230924054021/medias/110342-105-A_st_VF-MAL.vtt
    return