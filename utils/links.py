#################
#
# Video2Text
# By @DemangeJeremy
#
# - Links 
#
#################

from urllib.parse import urlparse

def getHost(link):
    try:
        parsed_link = urlparse(link)
        parsed_link = (parsed_link.netloc).replace('www.', '')
    except:
        return link
    return parsed_link